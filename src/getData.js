var request = require('request');
const { Client } = require('pg');

// connect to db
exports.getData = function (req, res) {
    const client = new Client({
        connectionString: process.env.DATABASE_URL || 'postgres://vmgnebirupirze:07330ccd94e6264cfb3ce0d3cb6100ea5c56f63e129f4ff4707a9a0389136f22@ec2-54-235-109-37.compute-1.amazonaws.com:5432/d3lkektg2uvabm',
        ssl: true,
    });
    client.connect();

// get all books
    let tasks = [];
    tasks.push(new Promise((resolve, reject) => {
        let sql = 'SELECT * FROM Books WHERE 1 = 1';
        let args = [];
        if (req.query.book_name) {
            sql += ' AND book_name LIKE $1';
            args.push('%' + req.query.book_name + '%');
        }
        
        client.query(sql, args, (err, dbRes) => {
            if (err) reject(err);
    
            resolve(dbRes.rows);
        });
    }));
    // if detect uid --> get their favourite books
    if (req.query.uid) {
        tasks.push(new Promise((resolve, reject) => {
            client.query('SELECT * FROM Favourite WHERE uid = $1', [req.query.uid], (err, dbRes) => {
                if (err) reject(err);

                resolve(dbRes.rows);
            });
        }));
    }


    // do both things (upper)
    Promise.all(tasks)
    .then((values) => {
        client.end();

        let books = values[0];

    // if user have login
        if (req.query.uid) {
            let favourites = values[1];
            
            for (let i = 0; i < books.length; i++) {
                books[i].favourite = false;
                
                for (let j = 0; j < favourites.length; j++) {
                    if (favourites[j].bid == books[i].id) {
                        books[i].favourite = true;
                        break;
                    }
                }
            }
        }

        //Hypermedia As The Engine Of Application State (HATEOAS)
        res.json({
            success: true,
            data: books,
            insertUrl: 'https://jeffreycls.herokuapp.com/book',
            updateUrl: 'https://jeffreycls.herokuapp.com/book',
            deleteUrl: 'https://jeffreycls.herokuapp.com/book',
            updateDbUrl: 'https://jeffreycls.herokuapp.com/updateDb'
        });
    })
    .catch((err) => {
        console.log(err);  
    });
};
