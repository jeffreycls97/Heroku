'use strict';

angular.module('myApp.login', ['ngRoute', 'ngMaterial'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/login', {
    templateUrl: 'login/login.html',
    controller: 'LoginCtrl'
  });
}])

// get auth from firebase and do create user or login
.controller('LoginCtrl', function($scope, $firebaseAuth, $http) {
  $scope.authObj = $firebaseAuth();
  
  $scope.email = null;
  $scope.password = null;

// create a new user 
  function createUser(user) {
    return $http.post('/createUser', {uid: user.uid, email: user.email});
  }
  
  function onLogin(firebaseUser) {
    console.log(firebaseUser);
  }

  function onLoginError(error) {
    console.log(error);
  }
  
  //login function
  $scope.emailLogin = function() {
    var email = $scope.email;
    var password = $scope.password;
    
    // create user and save to firebase
    if (email && password) {
      $scope.authObj.$createUserWithEmailAndPassword(email, password)
          .then(function(firebaseUser) {
            createUser(firebaseUser)
                .then(function() {
                  onLogin(firebaseUser);
                });
          })
          .catch(function(error) {
              
              // check if error code is 'auth/email-already-in-use' -> do login function
            if (error.code = 'auth/email-already-in-use') {
              $scope.authObj.$signInWithEmailAndPassword(email, password)
                  .then(function(firebaseUser) {
                    onLogin(firebaseUser);
                  })
                  .catch(function(error) {
                    onLoginError(error);
                  });
              
              return;
            }
            
            onLoginError(error);
          });
    }
  };
  
  
  // sign out function
  $scope.logout = function() {
    $scope.authObj.$signOut();
  };
});
