'use strict';

angular.module('myApp.view2', ['ngRoute', 'ngMaterial'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/view2', {
    templateUrl: 'view2/view2.html',
    controller: 'View2Ctrl'
  });
}])

// get auth from firebase
.controller('View2Ctrl', function($scope, $http, $firebaseAuth, $mdDialog) {
  $scope.authObj = $firebaseAuth();
  $scope.firebaseUser = $scope.authObj.$getAuth();
  $scope.authObj.$onAuthStateChanged(function(firebaseUser) {
    if (firebaseUser) {
      $scope.firebaseUser = firebaseUser;

      init();
    }
  });

  $scope.favourites = [];

 // remove Favourite function
  $scope.removeFavourite = function(bid, listId) {
    $http.post('/removeFavourite', {uid: $scope.firebaseUser.uid, bid: bid})
        .then(function(res) {
          return res.data;
        })
        .then(function(res) {
          console.log(res);

          $scope.favourites.splice(listId, 1);
        })
        .catch(function(err) {
          console.log(err);
        });
  };

// // edit Favourite notes function
  $scope.editNotes = function(bid, oldNotes, listId, ev) {
    var confirm = $mdDialog.prompt()
        .title('Add Notes')
        .placeholder('Notes')
        .ariaLabel('Notes')
        .initialValue(oldNotes)
        .targetEvent(ev)
        .ok('Save')
        .cancel('Cancel');

    $mdDialog.show(confirm)
        .then(function(notes) {
          
          // set favourite notes to db with uid, bid and notes
          $http.post('setFavouriteNotes', {uid: $scope.firebaseUser.uid, bid: bid, notes: notes})
              .then(function(res) {
                return res.data;
              })
              .then(function(res) {
                console.log(res);

                // display the newest notes on the web
                $scope.favourites[listId].notes = notes;
              })
              .catch(function(err) {
                console.log(err);
              });
        })
        .catch(function() {
          
        });
  };
  
  
  // get favourite books by uid
  function init() {
    $http.get('/getFavourite?uid=' + $scope.firebaseUser.uid)
        .then(function(res) {
          return res.data;
        })
        .then(function(res) {
          $scope.favourites = res.data;
        })
        .catch(function(err) {
          console.log(err);
        });
  }
});