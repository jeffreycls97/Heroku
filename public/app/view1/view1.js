'use strict';

angular.module('myApp.view1', ['ngRoute', 'ngMaterial'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/view1', {
    templateUrl: 'view1/view1.html',
    controller: 'View1Ctrl'
  });
}])


// get auth from firebase
.controller('View1Ctrl', function($scope, $http, $firebaseAuth) {
  $scope.authObj = $firebaseAuth();
  $scope.firebaseUser = $scope.authObj.$getAuth();
  $scope.authObj.$onAuthStateChanged(function(firebaseUser) {
    if (firebaseUser) {
      $scope.firebaseUser = firebaseUser;

      init();
    }
  });
  
  $scope.books = [];
  $scope.searchName = '';

  // search function
  $scope.search = function() {
    var searchName = $scope.searchName;
    
    // search book with my own api
    var url = '/book?book_name=' + searchName;

    if ($scope.firebaseUser) {
      url += '&uid=' + $scope.firebaseUser.uid;
    }

    $http.get(url)
      .then(function(res) {
        return res.data;
      })
      .then(function(res) {
        $scope.books = res.data;
      });
  };

  // Update Favourite function
  $scope.updateFavourite = function(bid, favourite) {
    if ($scope.firebaseUser) {
      if (favourite) {
        
        // add favourite with uid and bid 
        $http.post('/addFavourite', {uid: $scope.firebaseUser.uid, bid: bid})
            .then(function(res) {
              return res.data;
            })
            .then(function(res) {
              console.log(res);
            })
            .catch(function(err) {
              console.log(err);
            });
      } else {
        
        // remove favourite with uid and bid 
        $http.post('/removeFavourite', {uid: $scope.firebaseUser.uid, bid: bid})
            .then(function(res) {
              return res.data;
            })
            .then(function(res) {
              console.log(res);
            })
            .catch(function(err) {
              console.log(err);
            });
      }
    }
  };

// get books with or withnot user login
  function init() {
    var url = '/book';

    if ($scope.firebaseUser) {
      url += '?uid=' + $scope.firebaseUser.uid;
    }

    $http.get(url)
        .then(function(res) {
          return res.data;
        })
        .then(function(res) {
          $scope.books = res.data;
        })
        .catch(function(err) {
          console.log(err);
        });
  }

  init();
});