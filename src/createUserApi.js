const {Client} = require('pg');

exports.createUser = function(req, res) {
    const client = new Client({
        connectionString: process.env.DATABASE_URL || 'postgres://vmgnebirupirze:07330ccd94e6264cfb3ce0d3cb6100ea5c56f63e129f4ff4707a9a0389136f22@ec2-54-235-109-37.compute-1.amazonaws.com:5432/d3lkektg2uvabm',
        ssl: true
    });
    client.connect();

    let uid = req.body.uid;
    let email = req.body.email;
    
    client.query('INSERT INTO Users (uid, email) VALUES ($1, $2)', [uid, email], (err, dbRes) => {
        if (err) res.json({ success: false });

        client.end();

        res.json({
            success: true,
            uid: uid,
            email: email
        });
    });
};
