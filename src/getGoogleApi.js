var rp = require('request-promise');
var async = require('async');
const { Client } = require('pg');

exports.getBooks = function (req, res) {
    // get into DB
    const client = new Client({
        connectionString: process.env.DATABASE_URL || 'postgres://vmgnebirupirze:07330ccd94e6264cfb3ce0d3cb6100ea5c56f63e129f4ff4707a9a0389136f22@ec2-54-235-109-37.compute-1.amazonaws.com:5432/d3lkektg2uvabm',
        ssl: true,
    });
    client.connect();

    const key = 'AIzaSyBpbm5K7rcvm_TOG2Uh4GrljA50zWc7mfU';
    
    rp('https://www.googleapis.com/books/v1/volumes?q=' + req.query.q + '&key=' + key)
            .then(function(data) {
                const jsonData = JSON.parse(data);
                const books = jsonData.items;

                //
                let tasks = [];
                for (let i = 0; i < books.length; i++) {
                    if (!books[i].volumeInfo.title || !books[i].volumeInfo.authors || books[i].volumeInfo.authors.length === 0) continue;
                    let book_name = books[i].volumeInfo.title;
                    let author = books[i].volumeInfo.authors[0];
                    
                    tasks.push(function(callback) {
                        client.query("INSERT INTO Books (book_name, author) VALUES ($1, $2);", [book_name, author])
                                .then(function(dbRes) {
                                    callback(null);
                                })
                                .catch(function(dbErr) {
                                    callback(dbErr);
                                });
                    });
                }

                async.series(tasks, function(dbErr) {
                    client.end();
                    
                    if (dbErr) {
                        console.log(dbErr);
                        res.json({success: false});
                        return;
                    }
                    
                    res.json({success: true, getUrl: 'https://jeffreycls.herokuapp.com/book'});
                });
            })
            .catch(function(err) {
                console.log(err);
                res.json({success: false});
            });
};
