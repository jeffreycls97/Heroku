const {Client} = require('pg');


// get favourite books
exports.getFavourite = function(req, res) {
    let uid = req.query.uid;
    
    // if no uid return false
    if (!uid) {
        res.json({success: false});
    }
    
    // connect db
    const client = new Client({
        connectionString: process.env.DATABASE_URL || 'postgres://vmgnebirupirze:07330ccd94e6264cfb3ce0d3cb6100ea5c56f63e129f4ff4707a9a0389136f22@ec2-54-235-109-37.compute-1.amazonaws.com:5432/d3lkektg2uvabm',
        ssl: true
    });
    client.connect();

    // get the bookid by userid
    client.query('SELECT Books.*, Favourite.notes FROM Favourite, Books WHERE Favourite.bid = Books.id AND uid = $1', [uid], (err, dbRes) => {
        if (err) res.json({ success: false });

        let favourites = [];

        for (let row of dbRes.rows) {
            favourites.push(row);
        }

        client.end();

        res.json({
            success: true,
            data: favourites
        });
    });
};


// add favourite books
exports.addFavourite = function(req, res) {
    let uid = req.body.uid;
    let bid = req.body.bid;

    if (!uid || !bid) {
        res.json({success: false});
    }
    
    // connect db
    const client = new Client({
        connectionString: process.env.DATABASE_URL || 'postgres://vmgnebirupirze:07330ccd94e6264cfb3ce0d3cb6100ea5c56f63e129f4ff4707a9a0389136f22@ec2-54-235-109-37.compute-1.amazonaws.com:5432/d3lkektg2uvabm',
        ssl: true
    });
    client.connect();

    // insert the uid and bid to db
    client.query('INSERT INTO Favourite (uid, bid) VALUES ($1, $2)', [uid, bid], (err, dbRes) => {
        if (err) res.json({success: false});

        client.end();

        res.json({
            success: true,
            uid: uid,
            bid: bid
        });
    });
};

// remove favourite books
exports.removeFavourite = function(req, res) {
    let uid = req.body.uid;
    let bid = req.body.bid;

    if (!uid || !bid) {
        res.json({success: false});
    }
    
    // connect db
    const client = new Client({
        connectionString: process.env.DATABASE_URL || 'postgres://vmgnebirupirze:07330ccd94e6264cfb3ce0d3cb6100ea5c56f63e129f4ff4707a9a0389136f22@ec2-54-235-109-37.compute-1.amazonaws.com:5432/d3lkektg2uvabm',
        ssl: true
    });
    client.connect();

    // delete favourite books from db by uid and bid
    client.query('DELETE FROM Favourite WHERE uid = $1 AND bid = $2', [uid, bid], (err, dbRes) => {
        if (err) res.json({success: false});

        client.end();

        res.json({
            success: true
        });
    });
};

// set notes to favourite books
exports.setFavouriteNotes = function(req, res) {
    let uid = req.body.uid;
    let bid = req.body.bid;
    let notes = req.body.notes;

    if (!uid || !bid) {
        res.json({success: false});
    }
    
    // connect db
    const client = new Client({
        connectionString: process.env.DATABASE_URL || 'postgres://vmgnebirupirze:07330ccd94e6264cfb3ce0d3cb6100ea5c56f63e129f4ff4707a9a0389136f22@ec2-54-235-109-37.compute-1.amazonaws.com:5432/d3lkektg2uvabm',
        ssl: true
    });
    client.connect();

    // update notes 
    client.query('UPDATE Favourite SET notes = $1 WHERE uid = $2 AND bid = $3', [notes, uid, bid], (err, dbRes) => {
        if (err) res.json({success: false});

        client.end();

        res.json({
            success: true
        });
    });
};
