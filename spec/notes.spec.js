describe('notes module', function () {
    beforeEach(function() {
  notes.clear();
  notes.add('first notes');
  notes.add('second notes');
  notes.add('third notes');
  notes.add('fourth notes');
  notes.add('fifth notes');
    });
 it("should be able to add a new note", function () {
    expect(notes.add('sixth note')).toBe(true);
    expect(notes.count()).toBe(6);
});

    it('should ignore blank notes');
    it('should ignore notes containing only whitespace');
    it('should require a string parameter');

});

