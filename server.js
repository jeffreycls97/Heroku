var http = require('http');
var express = require('express');
var bodyParser = require('body-parser');

var getDataApi = require('./src/getData.js');
var postDataApi = require('./src/postData.js');
var putDataApi = require('./src/putData.js');
var deleteDataApi = require('./src/deleteData.js');

var getGoogleApi = require('./src/getGoogleApi.js');

var createUserApi = require('./src/createUserApi.js');
var favouriteApi = require('./src/favouriteApi.js');

/*var server = http.createServer(function(req, res){
        res.writeHead(200);
        res.end("hello")
}).listen(process.env.PORT || 3000);*/
const app = express();

app.use(express.static('public/app'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get('/book', getDataApi.getData);
app.post('/book', postDataApi.postData);
app.put('/book', putDataApi.putData);
app.delete('/book', deleteDataApi.deleteData);

app.post('/updateDb', getGoogleApi.getBooks);

app.post('/createUser', createUserApi.createUser);
app.get('/getFavourite', favouriteApi.getFavourite);
app.post('/addFavourite', favouriteApi.addFavourite);
app.post('/removeFavourite', favouriteApi.removeFavourite);
app.post('/setFavouriteNotes', favouriteApi.setFavouriteNotes);

app.listen(process.env.PORT || 3000, () => console.log('Example app listening on port 3000!'));
